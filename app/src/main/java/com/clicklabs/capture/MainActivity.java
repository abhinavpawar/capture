package com.clicklabs.capture;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class MainActivity extends Activity {
    final int CAMERA_CAPTURE = 1;
    final String PICTURE_SELECT_TITLE = "Add Photo!";
    final String PICTURE_SELECT_OPTION_1 = "Take Photo";
    final String PICTURE_SELECT_OPTION_2 = "Choose from Library";
    final String PICTURE_SELECT_OPTION_3 = "Cancel";
    final String CAMERA_CAPTURE_NOT_SUPPORTED="This device doesn't support the camera capture!";
    final int SELECT_FILE = 5;
    final String PROFILE_PICTURE_PATH = "/sdcard/Capital One/";
    final int CROP_PIC = 2;
    final String FOLDER_NAME = "/Capital One";
    final String CROP_NOT_SUPPORTED_PROMPT="This device doesn't support the crop action!";
    final int REQUEST_CAMERA = 4;
    private Uri picUri;
    ImageView customerImage;
    String picturePath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        customerImage = (ImageView) findViewById(R.id.image);

        customerImage.setImageResource(R.mipmap.ic_launcher);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_CAPTURE || requestCode == SELECT_FILE) {
                // get the Uri for the captured image
                picUri = data.getData();
                performCrop();
            } else if (requestCode == CROP_PIC) {
                // get the returned data
                Bundle extras = data.getExtras();
                // get the cropped bitmap
                Bitmap thePic = extras.getParcelable("data");


                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thePic.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                //thePic = getCroppedBitmap(thePic);

                File direct = new File(Environment.getExternalStorageDirectory() + FOLDER_NAME);

                if (!direct.exists()) {
                    File wallpaperDirectory = new File(PROFILE_PICTURE_PATH);
                    wallpaperDirectory.mkdirs();
                }

                File destination = new File(Environment.getExternalStorageDirectory() + FOLDER_NAME,
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                picturePath = destination.getAbsolutePath();
                Bitmap temp = Bitmap.createBitmap(BitmapFactory.decodeFile(picturePath));
                temp = getCroppedBitmap(temp);
                // Bitmap temporary=getCroppedBitmap(BitmapFactory.decodeFile(picturePath));
                //customerImage.setImageBitmap(thePic);
                customerImage.setImageBitmap(temp);
            }
        }

    }

    private void performCrop() {
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 512);
            cropIntent.putExtra("outputY", 512);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, CROP_PIC);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            Toast toast = Toast
                    .makeText(this, CROP_NOT_SUPPORTED_PROMPT, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    public void picOnClick(View v) {
        final CharSequence[] items = {PICTURE_SELECT_OPTION_1, PICTURE_SELECT_OPTION_2,
                PICTURE_SELECT_OPTION_3};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(PICTURE_SELECT_TITLE);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(PICTURE_SELECT_OPTION_1)) {
                    try {
                        // use standard intent to capture an image
                        Intent captureIntent = new Intent(
                                MediaStore.ACTION_IMAGE_CAPTURE);
                        // we will handle the returned data in onActivityResult
                        startActivityForResult(captureIntent, CAMERA_CAPTURE);
                    } catch (ActivityNotFoundException anfe) {
                        Toast toast = Toast.makeText(getApplicationContext(), CAMERA_CAPTURE_NOT_SUPPORTED, Toast.LENGTH_SHORT);
                        toast.show();
                    }

                } else if (items[item].equals(PICTURE_SELECT_OPTION_2)) {
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(i, SELECT_FILE);
                } else if (items[item].equals(PICTURE_SELECT_OPTION_3)) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }
}